- Patch: https://github.com/LukeZGD/iOS-OTA-Downgrader-Keys/releases/download/jailbreak/untether.patch
- Commands:

```
# Patch
bspatch untether.bin untethermod.bin untether.patch
# Get diff
xxd untether.bin u1.hex
xxd untethermod.bin u2.hex
diff u1.hex u2.hex
```

- Diff:

```
2015c2015
< 00007de0: 7073 210a 0077 005b 215d 2043 616e 2774  ps!..w.[!] Can't
---
> 00007de0: 7073 210a 0072 005b 215d 2043 616e 2774  ps!..r.[!] Can't
2203,2204c2203,2204
< 000089a0: 302e 7265 6c6f 6164 2e70 6c69 7374 0072  0.reload.plist.r
< 000089b0: 6d20 2f76 6172 2f6c 6f67 732f 756e 7465  m /var/logs/unte
---
> 000089a0: 302e 7265 6c6f 6164 2e70 6c69 7374 003a  0.reload.plist.:
> 000089b0: 2020 2f76 6172 2f6c 6f67 732f 756e 7465    /var/logs/unte
```
